package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	suffix := ""
	if len(os.Args) > 1 {
		suffix = os.Args[1]
	}

	err := run(suffix)
	if err != nil {
		log.Fatal(err)
	}
}

func run(suffix string) error {
	for i := 0; i < 100_000; i++ {
		if i%10_000 == 0 {
			log.Printf("file #%d", i)
		}

		path := fmt.Sprintf("file_%d_%s", i, suffix)
		content := path
		err := os.WriteFile(path, []byte(content), 0644)
		if err != nil {
			return err
		}
	}

	return nil
}

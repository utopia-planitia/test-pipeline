package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/containerd/platforms"
	"github.com/moby/buildkit/util/archutil"
	"github.com/moby/buildkit/util/bklog"
	ocispecs "github.com/opencontainers/image-spec/specs-go/v1"
)

// This test was introduced because when BuildKit is used to build ARM64 images in our pipelines it often fails to compile PHP extensions.
// The only consistent thing with the error messages is that the QEMU emulator (/dev/.buildkit_qemu_emulator) is used. This hints at missing support for foreign binaries in the kernel.
// See https://github.com/moby/buildkit/blob/master/docs/multi-platform.md.
func main() {
	amd64 := platforms.Normalize(ocispecs.Platform{
		Architecture: "amd64",
		OS:           "linux",
	})
	arm64 := platforms.Normalize(ocispecs.Platform{
		Architecture: "arm64",
		OS:           "linux",
	})
	expectedPlatforms := []ocispecs.Platform{amd64, arm64}

	// SupportedPlatforms is the function that BuildKit uses to determine if it needs to use the QEMU emulator as a fallback for building images for a specific architecture.
	// See https://github.com/moby/buildkit/blob/v0.18.0/solver/llbsolver/ops/exec_binfmt.go#L89
	supportedPlatforms := archutil.SupportedPlatforms(false)

	b, err := json.MarshalIndent(supportedPlatforms, "", "    ")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Supported Platforms:")
	fmt.Println(string(b))

	archutil.WarnIfUnsupported(expectedPlatforms)

	var isSupported bool
	for _, e := range expectedPlatforms {
		isSupported = false
		for _, s := range supportedPlatforms {
			if platforms.Only(s).Match(e) {
				isSupported = true
				break
			}
		}
		if !isSupported {
			bklog.L.Errorf("%s/%s is not supported", e.OS, e.Architecture)
			os.Exit(1)
		}
	}
}
